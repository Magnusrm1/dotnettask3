﻿using System;
using System.Collections.Generic;

namespace DotNetTask3
{
    class Program
    {
        public static void Search(string input, List<string> partialMatch, List<string> fullMatch, List<string> names)
        {
            // Find and store matching names.
            foreach (string item in names)
            {
                // If the name matches completely we store the name in fullMatch and
                // dont need it in partial matches.
                if (item == input)
                {
                    fullMatch.Add(item);
                }
                else if (item.Contains(input))
                {
                    partialMatch.Add(item);
                }
            }
        }

        public static void PrintResults(List<string> partialMatch, List<string> fullMatch)
        {
            // Display matching names.
            Console.WriteLine("Partial matches:");
            foreach (string item in partialMatch)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("\nFull matches:");
            foreach (string item in fullMatch)
            {
                Console.WriteLine(item);
            }
        }

        static void Main(string[] args)
        {
            List<string> names = new List<string>() { "Per Person", "Ola Nordmann", "Lise Olsen",
                                                      "Kari Nordmann", "Petter Skjerven"};
            List<string> partialMatch = new List<string>();
            List<string> fullMatch = new List<string>();

            while (true)
            {
                // Matches are cleared each iteration.
                partialMatch.Clear();
                fullMatch.Clear();

                Console.WriteLine("Search a name!");
                String input = Console.ReadLine();

                Search(input, partialMatch, fullMatch, names);

                PrintResults(partialMatch, fullMatch);
            }
        }
    }
}
